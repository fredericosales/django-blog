/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	 config.uiColor = '#000000';
};


CKEDITOR.on('instanceReady', function (ev) {
    ev.editor.dataProcessor.htmlFilter.addRules(
     {
        elements:
         {
           $: function (element) {
             // check for the tag name
             if (element.name == 'img') {
 
                 element.attributes.style = "height: auto; width: 100%;"
				         element.attributes.class = "zoeira";
             }
 
             // return element with class attribute
             return element;
          }
        }
    });
  });