#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""

"""


# imports
from django import template
from django.template.defaultfilters import stringfilter
import datetime


register = template.Library()


class Utilities(object):


    def __init__(self) -> None:
        pass
    

    @register.filter(name='cut')
    @stringfilter
    def cut(self, value, arg):
        """
        Removes all values from the givem string.
        Example:
        
        {{ somevariable|cut:"0" }}
        """
        return value.replace(arg, '')


    @register.filter(name='lower')
    @stringfilter
    def lower(self, value):
        """
        Converts a string to lower case.
        """
        return value.lower()


    @register.filter(name='upper')
    @stringfilter
    def upper(self, value):
        """
        Converts a string to lower case.
        """
        return value.upper()


    @register.simple_tag
    def current_time(self, format_string):
        """
        Returns the current_time as format string instructions.

        Example: 
        {% current_time "%Y-%m-%d %I:%M %p" as the_time %}
        <p>The time is {{ the_time }}.</p>
        """
        return datetime.datetime.now().strftime(format_string)
