#!/usr/bin/env python
# -*- coding: utf-8 -*-


from distutils.command.install_egg_info import safe_name
from django.contrib import admin
from blog.models import Category, News, Mail


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('category', 'date')

    class Media:
        js = (
            '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
            'js/script.min.js',
            'js/script.js', 
        )


class NewsAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'created', 'active')

    class Media:
        js = (
            '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
            'js/script.min.js',
            'js/script.js', 
        )


class MailAdmin(admin.ModelAdmin):
    list_display = ('user', 'port', 'smtp')

    class Media:
        js = (
            '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js',
            'js/script.min.js',
            'js/script.js', 
        )


# register
admin.site.site_header = 'Frederico Sales'
admin.site.register(Category, CategoryAdmin)
admin.site.register(News, NewsAdmin)
admin.site.register(Mail, MailAdmin)
