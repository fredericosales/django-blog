#!/usr/bin/env python
# -*- coding: utf-8 -*-


# imports
from email.policy import default
from logging import PlaceHolder
from django import forms
from blog.models import Mail


# combos
SEX = [
    ('m', 'masculino'),
    ('f', 'feminino'),
    ('l', 'lgbtqia+'),
    ('n', 'não declarado')
]


ETN = [
    ('b', 'branco'),
    ('p', 'preto'),
    ('a', 'pardo'),
    ('i', 'indio'),     
    ('m', 'amarelo'),
    ('n', 'não declarado')
]


NAC = [
    ('b', 'brasileira'),
    ('e', 'estrangeira')
]

PORTS = (
    ('default', '25'),
    ('TLS', '465'),
    ('SSL', '587'),
)


# forms class
class SendEmail(forms.Form):
    email    = forms.EmailField(required=True)
    smtp     = forms.CharField(max_length=128,  required=True)
    ports    = forms.ChoiceField(required=True, choices=PORTS)
    ssl      = forms.BooleanField()
    ttl      = forms.BooleanField()
    user     = forms.CharField(max_length=128, required=True)
    password = forms.CharField(widget=forms.PasswordInput)


class SendEmailModel(forms.ModelForm):
    class Meta():
        model = Mail
        fields = "__all__"
