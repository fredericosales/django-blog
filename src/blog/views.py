from django.shortcuts import render
from django.http import HttpResponse, FileResponse
import logging
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from blog.forms import Mail, SendEmail, SendEmailModel


# logger
LOGGER = logging.getLogger(__name__)


# blog application
def index(request):
    """
    index
    This view displays the bootstrap page
    as template.

    :param request: HttpRequest object.
    :return: HttpResponse template object.
    
    """

    return render(request, 'index.html')


def hello_mothafucka(request):
    """
    Hello mothafucka
    This view show a pdf file containing the
    initial information about the reportlab.
    https://docs.reportlab.com/reportlab/userguide/ch1_intro/

    :param request: HttpRequest object
    :return: HttpResponse pdf file as attachment.

    """

    buffer = io.BytesIO()

    # generate the freaking pdf.
    p = canvas.Canvas(buffer, pagesize=A4)
    p.drawString(100, 100, 'Hello Mothafucka.')
    p.showPage()
    p.save()

    buffer.seek(0)

    return FileResponse(buffer, as_attachment=True, filename='hello_mothafucka.pdf')


def FormSendMail(request):
    if request.method == 'GET':
        form = SendEmailModel()
        context = {
            'form': form,
        }
        return render(request, 'sendmail.html', context=context)
    else:   
        form = SendEmailModel(request.POST)
        if form.is_valid():
            mail_server = form.save()
            form = SendEmailModel()
        
        context = {
            'form': form,
        }
        return render(request, 'sendmail.html', context=context)
