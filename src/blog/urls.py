from django.urls import path, include
from blog import views
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('', views.index, name='index'),
    path('pdf', views.hello_mothafucka, name='pdf'),
    path('ckeditor', include('ckeditor_uploader.urls')),
    path('sendmail', views.FormSendMail, name='sendmail'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)