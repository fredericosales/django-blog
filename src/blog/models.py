#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from encrypted_model_fields.fields import EncryptedCharField


class Category(models.Model):
    id          = models.AutoField(primary_key=True, db_index=True, unique=True)
    category    = models.CharField(max_length=120, null=False)
    description = RichTextUploadingField()
    date        = models.DateTimeField(default=datetime.now())
    
    def __str__(self):
         return self.category

    class Meta: 
        verbose_name = "Category"
        verbose_name_plural = "Categorias"


class News(models.Model):   
    id = models.AutoField(primary_key=True, db_index=True, unique=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    author   = models.ForeignKey(User, on_delete=models.PROTECT)
    title    = models.CharField(max_length=120, null=False)
    text     = RichTextUploadingField()
    created  = models.DateTimeField(default=datetime.now())
    active   = models.BooleanField(default=False)

    def __str__(self):
         return self.title

    class Meta:
        verbose_name = "News"
        verbose_name_plural = "Notícias"

class Mail(models.Model):
    userid   = models.ForeignKey(User, on_delete=models.PROTECT)
    email    = models.EmailField(max_length=128, null=False)
    smtp     = models.CharField(max_length=128, null=False)
    port     = models.IntegerField(null=False)
    ssl      = models.BooleanField(default=False)
    ttl      = models.BooleanField(default=True)
    user     = models.CharField(max_length=128, null=False)
    password = EncryptedCharField(max_length=128, null=False)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = "Mail"
        verbose_name_plural = "Mail Server"