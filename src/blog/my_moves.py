#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
https://www.geeksforgeeks.org/setup-sending-email-in-django-project/

- subject refers to the email subject.
- message refers to the email message, the body of the email.
- email_from refers to the sender's details.This takes the EMAIL_HOST_USER from settings.py file, where you added those lines of code earlier.
- recipient_list is the list of recipients to whom the mail has to be sent that is, whoever registers to your application they receive the email.
- send_mail is an inbuilt Django function that takes subject, message, email_from, and recipient’s list as arguments, this is responsible to send emails.

"""


# import
from django.conf import settings
from django.core.mail import send_mail


# send mail to the specified mothafucka.
class SendEmail(object):
    """
    Class to send email.

    subject:        refers to the email subject.
    message:        refers to the email message, the body of the email.
    email_from:     refers to the sender.
    recipient_list: refers the list of recipients

    Use:

    MSG = \""" 
        Some message to send.

        Regards
        Me
        \"""

    sender = SendEmail(subject="Some theme", message=MSG, recipient=['dude@somedomain.io', 'otherdude@somedomain.io'])
    sender.send()

    Authors:        Frederico Sales, Bernard Rodrigues, Pedro Hote.
    Year:           2021.

    """
    def __init__(self, subject=None,  message=None, recipients=None, emailfrom=settings.EMAIL_HOST_USER ):
        self.subject = subject
        self.message = message
        self.emailfrom = emailfrom
        self.recipients = recipients


    def send(self):
        """
        return: send email to recipient(s).
        """
        return send_mail(self.subject, self.message, self.emailfrom, self.recipients)

